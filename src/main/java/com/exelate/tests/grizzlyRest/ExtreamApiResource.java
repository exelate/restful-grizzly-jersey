/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.tests.grizzlyRest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "api" path)
 * This class is only a proxy between the REST API functions exposed by Jersey
 * and the target class (e.g. ExtreamServer). Since this class is initiated by
 * Jersey at run-time and not by the programmer, you can't provide initial
 * parameters. You need to obtain a reference to the target class by using
 * a singleton.
 * 
 * @author mottyc
 */
@Path("/api")
public class ExtreamApiResource {

	
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
    
    /**
     * Method to start extream server processing
     * @return
     */
    @GET @Path("/start")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage startExtream() {
    	
    	// TODO: call a method on the target class
    	return new ResponseMessage(0, "Grizzly-Jersey-Extream: Started", 34);
    }
    
    /**
     * Method to stop extream server processing
     * @return
     */
    @GET @Path("/stop")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage stopExtream() {
    	
    	// TODO: call a method on the target class
    	return new ResponseMessage(0, "Grizzly-Jersey-Extream: Stopped", 45);
    }    
    
    /**
     * Method to reload extream configuration
     * @return
     */
    @GET @Path("/reload")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage reloadExtream() {
    	
    	// TODO: call a method on the target class
    	return new ResponseMessage(0, "Grizzly-Jersey-Extream: Reloaded", 58);
    }        
}
