/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.tests.grizzlyRest;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * Main embedded HTTP server that hosts Jersey.
 * Use this snippet inside your code (ExtreamServer) to start
 * listen to HTTP request. Don't forget to stop the server before
 * closing the Extream server process.
 *
 * @author mottyc
 */
public class StartGizzly {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8084/extream/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        
    	// create a resource config that scans for JAX-RS resources and providers
        // in current package
        final ResourceConfig rc = new ResourceConfig().packages("com.exelate.tests.grizzlyRest");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main entry point - only for test.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Grizzly hosting Jersey app listening at %s\n"
                + "Hit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }
}

