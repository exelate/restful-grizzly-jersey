/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.tests.grizzlyRest;

/**
 * The Class ResponseMessage.
 */
public class ResponseMessage {

	/********************************************************
	 * Return code
	 */
	private int code;
	public int getCode() { return code; }
	public void setCode(int value) { this.code = value; }

	/********************************************************
	 * Message
	 */
	private String message;
	public String getMessage() { return message; }
	public void setMessage(String value) { this.message = value; }

	/********************************************************
	 * ElapsedTime
	 */
	private long elapsedTime;
	public long getElapsedTime() { return elapsedTime; }
	public void setElapsedTime(long value) { this.elapsedTime = value; }
	
	/**
	 * Constructor
	 */
	public ResponseMessage() { }
	
	/**
	 * Instantiates a new response message.
	 *
	 * @param code the response code
	 * @param msg the message text
	 * @param elpTime the elapsed time
	 */
	public ResponseMessage(int code, String msg, long elpTime) { 
		this.setCode(code);
		this.setMessage(msg);
		this.setElapsedTime(elpTime);
	}

}
