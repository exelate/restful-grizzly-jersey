/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.tests.grizzlyRest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


// TODO: Auto-generated Javadoc
/**
 * The test case class demonstrating the use of the embedded API.
 *
 * @author mottyc
 */
public class ExtreamResourceTest {

	// Base address of the embedded server
	/** The Constant BASE_URI. */
	public static final String BASE_URI = "http://localhost:8084/extream/";
	
	// Start server REST method address
	/** The Constant START_URI. */
	public static final String START_URI = "http://localhost:8084/extream/api/start";
	
	// Stop server REST method address
	/** The Constant STOP_URI. */
	public static final String STOP_URI = "http://localhost:8084/extream/api/stop";
	
	// Reload server REST method address
	/** The Constant RELOAD_URI. */
	public static final String RELOAD_URI = "http://localhost:8084/extream/api/reload";
	
    /** The http server. */
    private HttpServer httpServer;
    
    /** The http client. */
    private CloseableHttpClient httpClient;

    /**
     * In the test setup, we will start our embedded server that starts listen
     * to HTTP requests. This is what you should do in your application bootstrap code
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        
    	// start the server
    	this.httpServer = StartGizzly.startServer();
        
        // create the client
    	this.httpClient = HttpClients.createDefault();
    }

    /**
     * In the test tear-down, we will stop our embedded server.
     * This is what you should do in your application exit code
     *
     * @throws Exception the exception
     */ 
    @After
    public void tearDown() throws Exception {
    	this.httpServer.stop();
    }

    /**
     * Test the call to the root API method: 
     * http://<server>:<port>/extream/api.
     */
    @Test
    public void testGetIt() {
        //String responseMsg = target.path("extream").request().get(String.class);
        //assertEquals("Got it!", responseMsg);
    }
    
    /**
     * Test the call to the start API method:
     * http://<server>:<port>/extream/api/start.
     */
    @Test
    public void testStart() {
    	testHttp(START_URI);
    }    
    
    /**
     * Test the call to the stop API method:
     * http://<server>:<port>/extream/api/stop.
     */
    @Test
    public void testStop() {
    	testHttp(STOP_URI);
    }   
    
    /**
     * Test the call to the stop API method:
     * http://<server>:<port>/extream/api/stop.
     */
    @Test
    public void testReload() {
    	testHttp(RELOAD_URI);
    }   
    
    /**
     * Test http invocation and check HTTP status code (200 = OK)
     * @param uri the uri to test
     */
    private void testHttp(String uri) {
     	try {
 			HttpGet request = new HttpGet(uri);
 			CloseableHttpResponse response = this.httpClient.execute(request);

			String body = EntityUtils.toString(response.getEntity());
			System.out.println(body);
			
			int rc = response.getStatusLine().getStatusCode();
			assertEquals(rc, 200);
			
			response.close();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
    }   
}
