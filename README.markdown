This project demonstrate how to expose REST service from your code using embedded container: [Grizzly](https://grizzly.java.net/J) and REST framework: [Jersey](https://jersey.java.net/).
The purpose of this project is for education only and it also utilizes maven 2.

## Documentation

Documentation will be provided soon.

## License

The use and distribution terms for this software are covered by the
Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
which can be found in the file LICENSE.html at the root of this distribution.
By using this software in any fashion, you are agreeing to be bound by
the terms of this license.
You must not remove this notice, or any other, from this software.

## Project lead

* Motty Cohen ([@exelate](mailto://mottyc@exelate.com))

## Contributors

* Still waiting for them...

